use tracing;
use tracing_subscriber::fmt::format::FmtSpan;


fn main() {
    let (non_blocking, _guard) = tracing_appender::non_blocking(std::io::stdout());
    tracing_subscriber::fmt()
        .with_timer(tracing_subscriber::fmt::time::time())
        .with_writer(non_blocking)
        //.with_ansi(true)
        //.with_file(true)
        //.with_line_number(true)
        //.compact()
        .pretty()
        .json()
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)

        // enable everything
        .with_max_level(tracing::Level::TRACE)
        // sets this to be the default, global collector for this application.
        .init();
    println!("Hello, world!");
    let a = "Aravind";
    //let encrypted_payload = "";
    //let v: Vec<&str> = encrypted_payload.split('.').collect();
    //let v2:Vec<_> = v.iter().map(|val| {base64::decode(val).unwrap()}).collect();
    tracing::event!(tracing::Level::DEBUG, id = "2", name = a, "1");
    //let span = tracing::span!(tracing::Level::TRACE, "main");
    //let _enter = span.enter();
    tracing::info!("i am here");
}

// enc/dec
// https://github.com/DaGenix/rust-crypto/blob/cc1a5fde1ce957bd1a8a2e30169443cdb4780111/src/aes_gcm.rs#L217

//fn find_all_valid_auth_token(now: SystemTime) -> Result<Vec<Row>, Error> {
    //let mut client = Client::connect("host=localhost user=postgres", NoTls)?;
    //client.query("SELECT * FROM AuthToken where token_validity = true AND token_expiry > $1 ", &[&now])
//}

//// select token_name, token_value from AuthToken where token_validity AND token_expiry > $1

//// for row in client.query("SELECT id, name, data FROM person", &[])? {
    ////let id: i32 = row.get(0);
    ////let name: &str = row.get(1);
    ////let data: Option<&[u8]> = row.get(2);

    ////println!("found person: {} {} {:?}", id, name, data);
//// }
